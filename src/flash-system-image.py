################################################################################
# Copyright 2021 ModalAI Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# 4. The Software is licensed to be used solely in conjunction with devices
#    provided by ModalAI Inc.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
################################################################################

'''
=======================================================================================================
flash-system-image

Description:
    Flashes system image binaries to supported qrb5165 device via fastboot

Usage: 
    flash-system-image.py [-h] [-v] [-f] [-d] [-c] [-o]

Optional Arguments:
  -h, --help        show this help message and exit
  -v, --version     show version and exit
  -f, --factory     flashes partition table as well as all partitions
  -d, --flash-data  flashes userdata partition (/data) along with the system flash
  -c, --flash-cal   flashes calibration partition (/data/modalai) along with the system flash
  -o, --flash-conf  flashes configuration partition (/etc/modalai) along with the system flash
=======================================================================================================
'''

#---------------------------------------------------------#
# Import libraries                                        #
#---------------------------------------------------------#
import os
import json
from platform import platform
import subprocess
from time import sleep
from argparse import ArgumentParser

# Define versions
TOOL_VERSION="0.3"
IMAGE_VERSION="1.1.4"

# Secondary Bootloader
XBL_CONFIG="xbl_config.elf"
XBL="xbl.elf"

# NHLOS
TZMBN="devcfg.mbn"
LUN0_PARTITION="gpt_both0.bin"
NHLOS="NON-HLOS.bin"

# HLOS
BOOTLOADER="abl.elf"
KERNEL="qti-ubuntu-robotics-image-qrb5165-rb5-boot.img"

# File systems
USRFS="qrb5165-data-fs.ext4"
CALFS="qrb5165-cal-fs.ext4"
CONFS="qrb5165-conf-fs.ext4"
SYSFS="qti-ubuntu-robotics-image-qrb5165-rb5-sysfs.ext4"

# parser args
TOOL_USAGE="flash-system-image.py [options]"
TOOL_DESCRIPTION="Flashes system image binaries to supported qrb5165 device via fastboot"

# Meta info, this will eventually be parsed from a file
VER_INFO="ver_info.txt"
PLATFORM = "" # No platform assumed as default

# ------------------------------------------------------- 
# flashing configuration, will be setup via opts and
# referenced throughout    
# ------------------------------------------------------- 
flash_conf = {
    "data_partition" : 0,
    "cal_partition"  : 0,
    "conf_partition" : 0,
    "lun0_partition" : 0,
}

# ------------------------------------------------------- 
# Print relevant versions                               
# ------------------------------------------------------- 
def _print_version() -> None:
    print(f"Installer Version: {TOOL_VERSION}")
    print(f"Image Version:     {IMAGE_VERSION}")

# ------------------------------------------------------- 
# argument parser                               
# ------------------------------------------------------- 
def _parse_opts() -> None:
    # Parse arguments
    ap = ArgumentParser(usage=TOOL_USAGE, description=TOOL_DESCRIPTION)
    ap.add_argument("-v", "--version", action='store_true', help="show version and exit")
    ap.add_argument("-f", "--factory", action='store_true', help="flashes partition table as well as all partitions")
    ap.add_argument("-d", "--flash-data", action='store_true', help="flashes userdata partition (/data) along with the system image flash")
    ap.add_argument("-c", "--flash-cal", action='store_true', help="flashes calibration partition (/data/modalai) along with the system image flash")
    ap.add_argument("-o", "--flash-conf", action='store_true', help="flashes configuration partition (/etc/modalai) along with the system image flash")

    # process arguments 
    args = ap.parse_args()
    if args.version:
        _print_version()
        exit(0)
    if args.flash_data:
        flash_conf["data_partition"] = 1
    if args.flash_cal:
        flash_conf["cal_partition"] = 1
    if args.flash_conf:
        flash_conf["conf_partition"] = 1
    if args.factory:
        # flashing factory requires flashing all other partitions as well
        flash_conf["data_partition"] = 1
        flash_conf["cal_partition"] = 1
        flash_conf["conf_partition"] = 1
        flash_conf["lun0_partition"] = 1

# ------------------------------------------------------- 
# fastboot flash given commands                               
# ------------------------------------------------------- 
def _fastboot_flash(partition: str, file: str) -> None:
    fastboot_command = "fastboot flash " + partition + " " + file
    print(fastboot_command)
    
    ret = subprocess.Popen(fastboot_command.split(" ")).wait()
    if ret != 0:
        print(f"[ERROR] Fastboot failed on the following command: {fastboot_command}")
        exit(0)

# ------------------------------------------------------- 
# Reboot fastboot       
# ------------------------------------------------------- 
def _fastboot_reboot(reboot_bootloader=0) -> None:
    fastboot_command = "fastboot reboot"
    if reboot_bootloader:
        fastboot_command += "-bootloader"

    ret = subprocess.Popen(fastboot_command.split(" ")).wait()
    if ret != 0:
        print(f"[ERROR] Fastboot failed on the following command: {fastboot_command}")
        exit(0)

# ------------------------------------------------------- 
# Misc helper tools
# ------------------------------------------------------- 
def _wait_for_device() -> None:
    adb_command = "adb wait-for-device"
    ret = subprocess.Popen(adb_command.split(" ")).wait()
    if ret != 0:
        print(f"[ERROR] Fastboot failed on the following command: {adb_command}")
        exit(0)

def _print_etc_version() -> None:
    version_command = "adb shell cat /etc/version"
    ret = subprocess.Popen(version_command.split(" ")).wait()
    if ret != 0:
        print(f"[ERROR] Fastboot failed on the following command: {version_command}")
        exit(0)

def _get_platform() -> None:
    with open(VER_INFO, 'r') as f:
        data = json.load(f)
        PLATFORM = data["ModalAI_Metabuild_Info"]["Platform"]

# ------------------------------------------------------- 
# Generate list of commands for fastboot to execute
# ------------------------------------------------------- 
def _flash_device() -> None:
    if flash_conf["lun0_partition"]:
        _fastboot_flash("partition:0", LUN0_PARTITION)
        _fastboot_reboot(1)
        sleep(10)

    # partitions that should be flashed every system image flash
    _fastboot_flash("xbl_config_a", XBL_CONFIG)
    _fastboot_flash("xbl_config_b", XBL_CONFIG)
    _fastboot_flash("xbl_a", XBL)
    _fastboot_flash("xbl_b", XBL)
    _fastboot_flash("modem_a", NHLOS)
    _fastboot_flash("modem_b", NHLOS)
    _fastboot_flash("devcfg_a", TZMBN)
    _fastboot_flash("devcfg_b", TZMBN)
    _fastboot_flash("boot_a", KERNEL)
    _fastboot_flash("boot_b", KERNEL)
    _fastboot_flash("system", SYSFS)

    # Optional partitions to flash 
    if flash_conf["data_partition"]:
        _fastboot_flash("userdata", USRFS)
    if flash_conf["conf_partition"]:
        _fastboot_flash("modalai_conf", CONFS)
    if flash_conf["cal_partition"]:
        _fastboot_flash("modalai_cal", CALFS)

# ------------------------------------------------------- 
# Check if binaries to be flashed are present
# ------------------------------------------------------- 
def _verify_binaries_exist() -> int:
    # Compile list of all files to flash
    files_list = [XBL_CONFIG, XBL, TZMBN, NHLOS, KERNEL, SYSFS, BOOTLOADER, VER_INFO]
    
    # Not all files are expected to be there, don't halt user for missing file
    # in situations where binary isn't required
    if flash_conf["data_partition"]:
        files_list.append(USRFS)

    if flash_conf["cal_partition"]:
        files_list.append(CALFS)

    if flash_conf["conf_partition"]:
        files_list.append(CONFS)

    if flash_conf["lun0_partition"]:
        files_list.append(LUN0_PARTITION)

    # Check if all files exist in file system
    for file in files_list:
        if not os.path.exists(file):
            print(f"[ERROR] file is missing: {file}")
            return -1

    print("[INFO] Found all required binaries")
    return 0

# ------------------------------------------------------- 
# Verify user has fastboot and device is in fastboot mode
# ------------------------------------------------------- 
def _put_device_in_fastboot() -> int:
    # try incase fastboot is not installed
    try:
        ret = subprocess.Popen(['fastboot', 'devices'], stdout=subprocess.PIPE).stdout.read().decode()
    except:
        print("[Error] android-tools-fastboot missing, use: apt-get install android-tools-fastboot")
        return -1

    # check if fastboot device populated serial number
    if ret.count('fastboot') >= 1:
        print("[INFO] Found fastboot device")
        return 0

    # Make sure user has adb
    try:
        ret = subprocess.Popen(['adb', 'devices'], stdout=subprocess.PIPE).stdout.read().decode()
    except:
        print("[Error] android-tools-adb missing, use: apt-get install android-tools-adb")
        return -1

    # Attempt to reboot into fastboot 
    try:
        ret = subprocess.check_output(['adb', 'reboot-bootloader'])
        sleep(3)
    except subprocess.CalledProcessError as ex:
        print("[Error] device is not in adb or in fastboot")
        return -1

    return 0

# ------------------------------------------------------- 
# Make sure the correct GPT partition layout is flashed  
# ------------------------------------------------------- 
def _verify_partitions() -> int:
    
    # M0052 and M0054 have different system partition sizes, check if M0052 else assume M0054
    sys_size = 0
    if PLATFORM == "M0054":
        sys_size = 0xD3B5D7000
    elif PLATFORM == "M0052":
        sys_size = 0xC2F5D7000
    else:
        print(f"[ERROR] platform {PLATFORM} is not supported. Must be either M0052 or M0054")
        return -1

    # define expected partitions and their sizes
    partitions = {
        "system"       : sys_size,
        "userdata"     : "0x1000000000",
        "modalai_cal"  : "0x4000000",
        "modalai_conf" : "0x4000000",
    }

    for partition in partitions.keys():
        fastboot_command = "fastboot getvar partition-size:" + partition
        fastboot_command = fastboot_command.split(" ")
        ret = subprocess.Popen(fastboot_command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT).stdout.read().decode()

        # Expected partition is missing, flash partition binary
        if ret.count('GetVar Variable Not found') > 0:
            print(f"[INFO] {partition} not found")
            return -1
        
        # Fastboot returns well-formed strings, extract size from string
        actual_part_size = ret.split("\n")[0]
        actual_part_size = actual_part_size.replace(f"partition-size:{partition}:  ", "")
        if actual_part_size != partitions[partition]:
            print(f"[INFO] {partition} partition returned size {actual_part_size}, expected {partitions[partition]}")
            return -1

    print("[INFO] Found correct partitions")
    return 0

# ------------------------------------------------------- 
# main fastboot logic                               
# ------------------------------------------------------- 
def main():
    _parse_opts()
    _print_version()

    # Verify fastboot is installed and device is in fastboot mode
    ret = _put_device_in_fastboot()
    if ret == -1:
        print("[INFO] can not continue with error, exiting")
        exit(ret)

    # Check if partitions are correct, they'll need to be reflashed if not
    if not flash_conf["lun0_partition"]:
        ret = _verify_partitions()
        if ret == -1:
            print("[ERROR] Partition layout was found to be incorrect, this requires flashing the lun0 partition table.")
            print("        Flashing the partition table will also flash every partition causing all data on the device")
            print("        to be lost. Make sure to backup any important files before doing. To flash the partition ")
            print("        table, rerun this script with the -f flag")
            exit(ret)    

    # Make sure required files are present
    ret = _verify_binaries_exist()
    if ret == -1:
        print("[INFO] can not continue with error, exiting")
        exit(ret)

    _get_platform() # Check which target is being flashed
    _flash_device()
    
    # Reboot device into adb and print relevant information
    _fastboot_reboot()
    _wait_for_device()
    print("[INFO] Device ready, version:")
    _print_etc_version()
    print("[INFO] Finished!")


if __name__ == "__main__":
    main()
