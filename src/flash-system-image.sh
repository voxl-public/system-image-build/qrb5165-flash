#!/bin/bash

################################################################################
# Copyright 2023 ModalAI Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# 4. The Software is licensed to be used solely in conjunction with devices
#    provided by ModalAI Inc.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
################################################################################
SET_BOLD="\033[1m"
RESET_ALL="\033[0m"

# -------------------------------------------------------
# Check OS to make sure it's compatible
# -------------------------------------------------------
case "$OSTYPE" in
    solaris*)
        echo "Detected unsupported OS: SOLARIS, exiting"
        exit -1
        ;;
    darwin*)
        echo "Detected OS: OSX"
        ;;
    linux*)
        echo "Detected OS: Linux"
        ;;
    bsd*)
        echo "Detected unsupported OS: BSD, exiting"
        exit -1
        ;;
    msys* | cygwin*)
        echo "Detected unsupported OS: WINDOWS, exiting"
        exit -1
        ;;
    *)
        echo "Detected unsupported OS: unknown: \"$OSTYPE\", exiting"
        exit -1
        ;;
esac

# ------------------------------------------------------- 
# Print relevant versions                               
# ------------------------------------------------------- 
_print_version() {
    echo "Installer Version: ${TOOL_VERSION}"
    echo "Image Version:     ${IMAGE_VERSION}"
    echo "Board Version:     ${BOARD_VERSION}"
}

_print_usage() {
    echo "======================================================================================================="
    echo "flash-system-image"
    echo ""
    echo "Description:"
    echo "    Flashes system image binaries to supported qrb5165 device via fastboot"
    echo ""
    echo "Usage: "
    echo "    flash-system-image.sh [-h] [-v]"
    echo ""
    echo "Optional Arguments:"
    echo "  -h, --help              show this help message and exit"
    echo "  -v, --version           show version and exit"
    echo "  -n, --non-interactive   don't ask questions, for scripted setup"
    echo "  -f, --factory           flash every partition including data,"
    echo "                          conf, and cal partitions"
    echo "  -k, --kernel            manually override kernel to be flashed,"
    echo "                          must pass in name of kernel file name"
    echo "======================================================================================================="
}

# m0054-1 or -2
# get here if we started flash in fastboot and can't detect QRB vs. QSM
_print_hw_rev_desc () {
    echo -e "\n======================================================================================================="
    echo -e "${SET_BOLD}BOARD IN FASTBOOT, CAN'T DETECT HARDWARE VERSION.${RESET_ALL}"
    echo -e "Please determine hardware version, ${SET_BOLD}-1${RESET_ALL} (QRB5165M) or ${SET_BOLD}-2${RESET_ALL} (QSM8250)\n"
    echo -e "See \033]8;;https://docs.modalai.com/m0054-versions\033\\docs.modalai.com/m0054-versions\033]8;;\033\\ for more info."
    echo -e "======================================================================================================="
}

# drone specific kernels mention m0173 camera breakout board
_print_kernel_options_desc () {
    echo -e "\n======================================================================================================="
    echo -e "In order to support all of VOXL's camera configurations, specific drones require differing kernel images." 
    echo -e "Please select your drone hardware to proceed with flashing the appropriate kernel image."
    echo -e "======================================================================================================="
}   

# ------------------------------------------------------- 
# global flags
# -------------------------------------------------------
factory_flash=false
NON_INTERACTIVE=""

# ------------------------------------------------------- 
# argument parser                               
# ------------------------------------------------------- 
_parse_opts() {
    while [[ $# -gt 0 ]]; do
        case $1 in
            -h|--help)
                _print_usage
                exit 0
                ;;
            -v|--version)
                _print_version
                exit 0
                ;;
            -f|--factory)
                # Enable factory flash mode which will flash every partition including data,
                # conf, and cal partitions which are meant to be persistent between regular flashes
                # This flag is meant to be hidden and only used by experienced user
                factory_flash=true
                ;;
            -n|--non-interactive)
                NON_INTERACTIVE="true"
                ;;
            -k|--kernel)
                kernel_override=true
                if [[ -z "$2" ]] || [[ "$2" =~ ^- ]]; then
                    echo "Error: Argument for $1 is missing" >&2
                    _print_usage
                    exit 1
                fi
                KERNEL_OVERRIDE_FILE="$2"
                shift  # Move past the value to the next option
                ;;
            *)
                echo "Unknown argument $1"
                __print_usage
                exit -1
                ;;
        esac
        shift
    done
}

_parse_opts $@

# -------------------------------------------------------
# Global defines
# -------------------------------------------------------

# Meta info
VER_INFO="ver_info.txt"

# Define versions
TOOL_VERSION="1.0"
IMAGE_VERSION="$( cat ${VER_INFO} | grep '"System_Image_Version":' | cut -d ':' -f 2 | cut -d '"' -f 2 )"
PLATFORM="$( cat ${VER_INFO} | grep '"Platform":' | cut -d ':' -f 2 | cut -d '"' -f 2 | tr '[:upper:]' '[:lower:]' )"

# Secondary Bootloader
XBL_CONFIG="xbl_config.elf"
XBL="xbl.elf"

# NHLOS
TZMBN="devcfg.mbn"
LUN0_PARTITION="gpt_both0.bin"
NHLOS="NON-HLOS.bin"
DSPSO="dspso.bin"

# HLOS
BOOTLOADER="abl.elf"

#
# TODO: improve, this is hardcoded for now to not break existing usage
#
machine_path="/sys/module/voxl_platform_mod/parameters/machine"
variant_path="/sys/module/voxl_platform_mod/parameters/variant"
config_path="/sys/module/voxl_platform_mod/parameters/config"
machine_version=$(adb shell "if [ -e \"$machine_path\" ]; then cat \"$machine_path\" | tr -d '[:space:]'; fi" 2>/dev/null)
variant_version=$(adb shell "if [ -e \"$variant_path\" ]; then cat \"$variant_path\" | tr -d '[:space:]'; fi" 2>/dev/null)
config_version=$(adb shell "if [ -e \"$config_path\" ]; then cat \"$config_path\" | tr -d '[:space:]'; fi" 2>/dev/null)

if [[ "$PLATFORM" == "m0054" ]] && [[ -n "$config_version" ]]; then # M0054 (config exists)
    if [[ "$variant_version" == "0" ]] && [[ "$config_version" == "0" ]]; then # var00.0
        echo -e "Detected: ${SET_BOLD}M0054-1${RESET_ALL} (QRB5165M)"
        echo -e "Detected: ${SET_BOLD}Config 0${RESET_ALL} (Starling, Sentinel, FPV V3/4, D0010, PX4 Dev Kit)"
        KERNEL="m0054-1-var00.0-kernel.img"
        TZMBN="devcfg-m0054-1-v8.mbn"
        BOARD_VERSION="M0054-1 (var00.0)"
    elif [[ "$variant_version" == "0" ]] && [[ "$config_version" == "1" ]]; then # var00.1
        echo -e "Detected: ${SET_BOLD}M0054-1${RESET_ALL} (QRB5165M)"
        echo -e "Detected: ${SET_BOLD}Config 1${RESET_ALL} w/ M0173 Breakout Board (Starling 2 Max, Starling 2, FPV V5)"
        KERNEL="m0054-1-var00.1-kernel.img"
        TZMBN="devcfg-m0054-1-v8.mbn"
        BOARD_VERSION="M0054-1 (var00.1)"
    elif [[ "$variant_version" == "2" ]] && [[ "$config_version" == "0" ]]; then # var02.0
        echo -e "Detected: ${SET_BOLD}M0054-2${RESET_ALL} (QSM8250)"
        echo -e "Detected: ${SET_BOLD}Config 0${RESET_ALL} (Starling, FPV V3/4)"
        KERNEL="m0054-2-var02.0-kernel.img"
        TZMBN="devcfg-m0054-2-v8.mbn"
        BOARD_VERSION="M0054-2 (var02.0)"
    elif [[ "$variant_version" == "2" ]] && [[ "$config_version" == "1" ]]; then # var02.1
        echo -e "Detected: ${SET_BOLD}M0054-2${RESET_ALL} (QSM8250)"
        echo -e "Detected: ${SET_BOLD}Config 1${RESET_ALL} w/ M0173 Breakout Board (Starling 2 Max, Starling 2, FPV V5)"
        KERNEL="m0054-2-var02.1-kernel.img"
        TZMBN="devcfg-m0054-2-v8.mbn"
        BOARD_VERSION="M0054-2 (var02.1)"   
    fi

elif [[ "$PLATFORM" == "m0054" ]] && [[ "$machine_version" == "1" ]]; then # config doesn't exist
    if [[ "$variant_version" == "0" ]]; then # var00.0
        echo -e "Detected: ${SET_BOLD}M0054-1${RESET_ALL} (QRB5165M)"
        KERNEL="m0054-1-var00.0-kernel.img"
        TZMBN="devcfg-m0054-1-v8.mbn"
        BOARD_VERSION="M0054-1 (var00.0)"
    else # var00.1
        echo -e "Detected: ${SET_BOLD}M0054-2${RESET_ALL} (QSM8250)"
        KERNEL="m0054-2-var02.0-kernel.img"
        TZMBN="devcfg-m0054-2-v8.mbn"
        BOARD_VERSION="M0054-2 (var02.0)"
    fi

elif [[ "$PLATFORM" == "m0104" ]] && [ "$machine_version" == "2" ]; then # M0104
    if [[ "$variant_version" == "0" ]] && [[ "$config_version" == "0" ]]; then # M0104-1, config 0
        echo -e "Detected: ${SET_BOLD}M0104-1${RESET_ALL} (QRB5165M)"
        echo -e "Detected: ${SET_BOLD}Config 0${RESET_ALL}"
        KERNEL="qti-ubuntu-robotics-image-m0104-1-boot.img"
        TZMBN="devcfg-m0104-1-v8.mbn"
        BOARD_VERSION="M0104-1 (Config 0)"
    elif [[ "$variant_version" == "0" ]] && [[ "$config_version" == "2" ]]; then # M0104-1, config 2
        echo -e "Detected: ${SET_BOLD}M0104-1${RESET_ALL} (QRB5165M)"
        echo -e "Detected: ${SET_BOLD}Config 2${RESET_ALL}"
        KERNEL="m0104-1-var00.2-kernel.img"
        TZMBN="devcfg-m0104-1-v8.mbn"
        BOARD_VERSION="M0104-1 (Config 2)"
    elif [[ "$variant_version" == "1" ]] && [[ "$config_version" == "0" ]]; then # M0104-2, config 0
        echo -e "Detected: ${SET_BOLD}M0104-2${RESET_ALL} (QSM8250)"
        echo -e "Detected: ${SET_BOLD}Config 0${RESET_ALL}"
        KERNEL="qti-ubuntu-robotics-image-m0104-2-boot.img"
        TZMBN="devcfg-m0104-2-v8.mbn"
        BOARD_VERSION="M0104-2 (Config 0)"
    elif [[ "$variant_version" == "1" ]] && [[ "$config_version" == "2" ]]; then # M0104-2, config 2
        echo -e "Detected: ${SET_BOLD}M0104-2${RESET_ALL} (QSM8250)"
        echo -e "Detected: ${SET_BOLD}Config 2${RESET_ALL}"
        KERNEL="m0104-2-var02.2-kernel.img"
        TZMBN="devcfg-m0104-2-v8.mbn"
        BOARD_VERSION="M0104-2 (Config 2)"
    fi


elif [[ $(adb devices | wc -l) -gt 2 ]]; then # adb detected, device is pre-1.7.4 system image
    if [[ "$PLATFORM" == "m0054" ]]; then # m0054-1 (var00.0)
        echo -e "Detected: ${SET_BOLD}M0054-1${RESET_ALL} (QRB5165M)"
        KERNEL="m0054-1-var00.0-kernel.img"
        TZMBN="devcfg-m0054-1-v8.mbn"
        BOARD_VERSION="$(tr '[:lower:]' '[:upper:]' <<< ${PLATFORM:0:1})${PLATFORM:1}-1 (var00.0)"
    else # m0104-1 
        echo -e "Detected: ${SET_BOLD}M0104-1${RESET_ALL} (QRB5165M)"
        KERNEL="qti-ubuntu-robotics-image-${PLATFORM}-1-boot.img"
        TZMBN="devcfg-${PLATFORM}-1-v8.mbn"
        BOARD_VERSION="$(tr '[:lower:]' '[:upper:]' <<< ${PLATFORM:0:1})${PLATFORM:1}-1"
    fi

else # Can't detect device version -> device is in fastboot
    echo ""
fi

if [ ! $NON_INTERACTIVE ]; then
    if [[ "$KERNEL" != "" ]]; then
        # we know what the kernel is, give users options based off detected board
        _print_kernel_options_desc
        echo -e "\n${SET_BOLD}PLEASE SELECT WHICH KERNEL TO FLASH${RESET_ALL}"
        case "$BOARD_VERSION" in
            *"M0054-1"*)
                while true; do
                    echo -e "0. M0054-1 -> QRB5165M, ${SET_BOLD}Starling${RESET_ALL} (D0005), ${SET_BOLD}Sentinel${RESET_ALL} (D0006), ${SET_BOLD}FPV V4${RESET_ALL} (D0008), ${SET_BOLD}D0010${RESET_ALL}, ${SET_BOLD}PX4 Dev Kit${RESET_ALL} (D0011)"
                    echo -e "1. M0054-1 -> QRB5165M w/ ${SET_BOLD}M0173 Camera Breakout Board${RESET_ALL}, ${SET_BOLD}Starling 2 Max${RESET_ALL} (D0012), ${SET_BOLD}Starling 2${RESET_ALL} (D0014), ${SET_BOLD}FPV V5${RESET_ALL} (D0008)"

                    # mention what kernel was detected (like in voxl-configure-sku)
                    echo -e "\nSelect your choice from the options above"
                    read -p "or press [Enter] to continue with current config: " choice
                    
                    if [ "$choice" == "0" ]; then
                        KERNEL="m0054-1-var00.0-kernel.img"
                        TZMBN="devcfg-m0054-1-v8.mbn"
                        BOARD_VERSION="M0054-1 (var00.0)"
                        break
                    elif [ "$choice" == "1" ]; then
                        KERNEL="m0054-1-var00.1-kernel.img"
                        TZMBN="devcfg-m0054-1-v8.mbn"
                        BOARD_VERSION="M0054-1 (var00.1)"
                        break
                    elif [ -z "$choice" ]; then
                        echo "No choice entered. Continuing without action..."
                        break
                    else
                        echo "Invalid option: $choice. Please try again."
                    fi
                done
                ;;
            *"M0054-2"*)
                while true; do
                    echo  -e "0. M0054-2 -> QSM8250, ${SET_BOLD}Starling${RESET_ALL} (D0005), ${SET_BOLD}FPV V4${RESET_ALL} (D0008)"
                    echo -e "1. M0054-2 -> QSM8250, ${SET_BOLD}Starling 2 Max${RESET_ALL} (D0012), ${SET_BOLD}Starling 2${RESET_ALL} (D0014), ${SET_BOLD}FPV V5${RESET_ALL} (D0008)"

                    # mention what kernel was detected (like in voxl-configure-sku)
                    read -p "Enter your choice (or press Enter to continue): " choice
                    
                    if [ "$choice" == "0" ]; then
                        KERNEL="m0054-2-var02.0-kernel.img"
                        TZMBN="devcfg-m0054-2-v8.mbn"
                        BOARD_VERSION="M0054-2 (var02.0)"
                        break
                    elif [ "$choice" == "1" ]; then
                        KERNEL="m0054-2-var02.1-kernel.img"
                        TZMBN="devcfg-m0054-2-v8.mbn"
                        BOARD_VERSION="M0054-2 (var02.1)"
                        break
                    elif [ -z "$choice" ]; then
                        echo "No choice entered. Continuing without action..."
                        break
                    else
                        echo "Invalid option: $choice. Please try again."
                    fi
                done
                ;;
            *"M0104-1"*)
                while true; do
                    echo -e "1. M0104-1 -> QRB5165M, ${SET_BOLD}VOXL 2 Mini${RESET_ALL}"
                    echo -e "2. M0104-1 -> QRB5165M w/ ${SET_BOLD}M0188 Camera Breakout Board${RESET_ALL}, ${SET_BOLD}Stinger${RESET_ALL}, ${SET_BOLD}MVX-T0001${RESET_ALL}"

                    # mention what kernel was detected (like in voxl-configure-sku)
                    echo -e "\nSelect your choice from the options above"
                    read -p "or press [Enter] to continue with current config: " choice
                    
                    if [ "$choice" == "1" ]; then
                        KERNEL="qti-ubuntu-robotics-image-m0104-1-boot.img"
                        TZMBN="devcfg-m0104-1-v8.mbn"
                        BOARD_VERSION="M0104-1"
                        break
                    elif [ "$choice" == "2" ]; then
                        KERNEL="m0104-1-var00.2-kernel.img"
                        TZMBN="devcfg-m0104-1-v8.mbn"
                        BOARD_VERSION="M0104-1"
                        break
                    elif [ -z "$choice" ]; then
                        echo "No choice entered. Continuing without action..."
                        break
                    else
                        echo "Invalid option: $choice. Please try again."
                    fi
                done
                ;;
            *"M0104-2"*)
                while true; do
                    echo -e "1. M0104-2 -> QSM8250, ${SET_BOLD}VOXL 2 Mini${RESET_ALL}"
                    echo -e "2. M0104-2 -> QSM8250 w/ ${SET_BOLD}M0188 Camera Breakout Board${RESET_ALL}, ${SET_BOLD}Stinger${RESET_ALL}, ${SET_BOLD}MVX-T0001${RESET_ALL}"

                    # mention what kernel was detected (like in voxl-configure-sku)
                    echo -e "\nSelect your choice from the options above"
                    read -p "or press [Enter] to continue with current config: " choice
                    
                    if [ "$choice" == "1" ]; then
                        KERNEL="qti-ubuntu-robotics-image-m0104-2-boot.img"
                        TZMBN="devcfg-m0104-2-v8.mbn"
                        BOARD_VERSION="M0104-2"
                        break
                    elif [ "$choice" == "2" ]; then
                        KERNEL="m0104-2-var02.2-kernel.img"
                        TZMBN="devcfg-m0104-2-v8.mbn"
                        BOARD_VERSION="M0104-1"
                        break
                    elif [ -z "$choice" ]; then
                        echo "No choice entered. Continuing without action..."
                        break
                    else
                        echo "Invalid option: $choice. Please try again."
                    fi
                done
                ;;
            *)
                echo "Substring not found."
                ;;
        esac

    else
        # we don't what the kernel is, give full list of options
        # Prompt user to choose device version to flash
        _print_hw_rev_desc
        _print_kernel_options_desc

        echo -e "\n${SET_BOLD}PLEASE SELECT WHICH KERNEL TO FLASH${RESET_ALL}"
        while true; do
            echo -e "1. M0054-1 -> QRB5165M, ${SET_BOLD}Starling${RESET_ALL} (D0005), ${SET_BOLD}Sentinel${RESET_ALL} (D0006), ${SET_BOLD}FPV V4${RESET_ALL} (D0008), ${SET_BOLD}D0010${RESET_ALL}, ${SET_BOLD}PX4 Dev Kit${RESET_ALL} (D0011)"
            echo -e "2. M0054-1 -> QRB5165M w/ ${SET_BOLD}M0173 Camera Breakout Board${RESET_ALL}, ${SET_BOLD}Starling 2 Max${RESET_ALL} (D0012), ${SET_BOLD}Starling 2${RESET_ALL} (D0014), ${SET_BOLD}FPV V5${RESET_ALL} (D0008)"
            echo -e "3. M0054-2 -> QSM8250, ${SET_BOLD}Starling${RESET_ALL} (D0005), ${SET_BOLD}FPV V4${RESET_ALL} (D0008)"
            echo -e "4. M0054-2 -> QSM8250 w/ ${SET_BOLD}M0173 Camera Breakout Board${RESET_ALL}, ${SET_BOLD}Starling 2 Max${RESET_ALL} (D0012), ${SET_BOLD}FPV V5${RESET_ALL} (D0008)"
            echo -e "5. M0104-1 -> QRB5165M"
            echo -e "6. M0104-1 -> QRB5165M, ${SET_BOLD}Stinger${RESET_ALL} (D0013), ${SET_BOLD}MVX-T0001${RESET_ALL}"
            echo -e "7. M0104-2 -> QSM8250"
            echo -e "8. M0104-2 -> QSM8250, ${SET_BOLD}Stinger${RESET_ALL} (D0013), ${SET_BOLD}MVX-T0001${RESET_ALL}"
            echo ""
            read -p "Select your choice from above options: " choice
            input
            if [ "$choice" == "1" ]; then
                KERNEL="m0054-1-var00.0-kernel.img"
                TZMBN="devcfg-m0054-1-v8.mbn"
                BOARD_VERSION="M0054-1 (var00.0)"
                break
            elif [ "$choice" == "2" ]; then
                KERNEL="m0054-1-var00.1-kernel.img"
                TZMBN="devcfg-m0054-1-v8.mbn"
                BOARD_VERSION="M0054-1 (var00.1)"
                break
            elif [ "$choice" == "3" ]; then
                KERNEL="m0054-2-var02.0-kernel.img"
                TZMBN="devcfg-m0054-2-v8.mbn"
                BOARD_VERSION="M0054-2 (var02.0)"
                break
            elif [ "$choice" == "4" ]; then
                KERNEL="m0054-2-var02.1-kernel.img"
                TZMBN="devcfg-m0054-2-v8.mbn"
                BOARD_VERSION="M0054-2 (var02.1)"
                break
            elif [ "$choice" == "5" ]; then
                KERNEL="qti-ubuntu-robotics-image-m0104-1-boot.img"
                TZMBN="devcfg-m0104-1-v8.mbn"
                BOARD_VERSION="M0104-1"
                break
            elif [ "$choice" == "6" ]; then
                KERNEL="m0104-1-var00.2-kernel.img"
                TZMBN="devcfg-m0104-1-v8.mbn"
                BOARD_VERSION="M0104-1"
                break
            elif [ "$choice" == "7" ]; then
                KERNEL="qti-ubuntu-robotics-image-m0104-2-boot.img"
                TZMBN="devcfg-m0104-2-v8.mbn"
                BOARD_VERSION="M0104-2"
                break
            elif [ "$choice" == "8" ]; then
                KERNEL="m0104-2-var02.2-kernel.img"
                TZMBN="devcfg-m0104-2-v8.mbn"
                BOARD_VERSION="M0104-2"
                break
            else
                echo "Invalid option: $choice. Please try again."
            fi
        done
    fi
fi

# File systems
USRFS="${PLATFORM}-data-fs.ext4"
CALFS="${PLATFORM}-cal-fs.ext4"
CONFS="${PLATFORM}-conf-fs.ext4"
SYSFS="qti-ubuntu-robotics-image-${PLATFORM}-sysfs.ext4"


# -------------------------------------------------------
# Map partition names to file names
# Format: KEY::VALUE
# -------------------------------------------------------
partMap=(
    "xbl_config_a::${XBL_CONFIG}"
    "xbl_config_b::${XBL_CONFIG}"
    "xbl_a::${XBL}"
    "xbl_b::${XBL}"
    "abl_a::${BOOTLOADER}"
    "abl_b::${BOOTLOADER}"
    "modem_a::${NHLOS}"
    "modem_b::${NHLOS}"
    "devcfg_a::${TZMBN}"
    "devcfg_b::${TZMBN}"
    "dsp_a::${DSPSO}"
    "dsp_b::${DSPSO}"
    "boot_a::${KERNEL}"
    "boot_b::${KERNEL}"
    "system::${SYSFS}"
    "userdata::${USRFS}"
    "modalai_conf::${CONFS}"
    "modalai_cal::${CALFS}"
)


# -------------------------------------------------------
# List of partitions to be flashed
# -------------------------------------------------------
flash_list=()
# partitions that should be flashed every system image flash
flash_list+=("xbl_config_a")
flash_list+=("xbl_config_b")
flash_list+=("xbl_a")
flash_list+=("xbl_b")
flash_list+=("abl_a")
flash_list+=("abl_b")
flash_list+=("modem_a")
flash_list+=("modem_b")
flash_list+=("devcfg_a")
flash_list+=("devcfg_b")
flash_list+=("dsp_a")
flash_list+=("dsp_b")
flash_list+=("boot_a")
flash_list+=("boot_b")
flash_list+=("system")
flash_list+=("modalai_conf")






# ------------------------------------------------------- 
# Select font color for messages                              
# ------------------------------------------------------- 
function font_color() {
        case $1 in
                'R'|'r') echo -n -e "\033[1;31m";;
                "G"|'g') echo -n -e "\033[1;32m";;
                "Y"|'y') echo -n -e "\033[1;33m";;
                "B"|'b') echo -n -e "\033[1;36m";;
                "undrl") echo -n -e "\033[1;4m";;
                "bold")  echo -n -e "\033[1m";;
                      *) echo -n -e "\033[0m";;
        esac
}

# ------------------------------------------------------- 
# Utility functions for formatting and progress bar                               
# ------------------------------------------------------- 
__scrollable_clear_terminal() {

    local numrows=$(tput lines)

    echo

    local i
    for (( i=1 ; i < $numrows ; i++ )) ; do
        echo -e ${CLEAR_LINE}
    done

    __gotoxy 0 0
}

function cursor-position {
    local CURPOS
    read -sdR -p $'\E[6n' CURPOS
    CURPOS=${CURPOS#*[} # Strip decoration characters <ESC>[
    echo "${CURPOS}"    # Return position in "row;col" format
}

function cursor-row {
    local COL
    local ROW
    IFS=';' read -sdR -p $'\E[6n' ROW COL
    echo "${ROW#*[}"
}

function cursor-col {
    local COL
    local ROW
    IFS=';' read -sdR -p $'\E[6n' ROW COL
    echo "${COL}"
}
CLEAR_LINE="\033[2K"

__gotoxy(){

    echo -ne "\033[$2;$1H"
}

__print_progress_bar(){

    if [ $# -eq 3 ] ; then
        NUM_CHARS=$3
    else
        # 10 is number of characters in "](xxx%) " at the end of the line
        # plus one "[" at the beginning and one extra at the end so that
        # ctrl+c doesn't run over
        NUM_CHARS=$((COLUMNS - 10))
    fi

    ((PCT=$1*100/$2))
    ((VAL=$PCT*$NUM_CHARS/100))

    local STRING="\r["

    local i
    for i in $(seq 1 $NUM_CHARS); do

        if [ $VAL -ge $i ]; then
            STRING="${STRING}-"
        else
            STRING="${STRING} "
        fi
    done
    STRING="${STRING}]($(printf "%3d" $PCT)%)"
    echo -ne "$STRING"
}




# ------------------------------------------------------- 
# fastboot flash given commands                               
# ------------------------------------------------------- 
_fastboot_flash() {

    local output="$( fastboot flash $1 $2 2>&1 )"

    if echo "$output" | grep -q FAILED ; then
        font_color r
        echo ""
        echo "[ERROR] Failed to flash partition: $1"
        echo ""
        echo "$output"
        font_color
        exit -1
    fi
}

# ------------------------------------------------------- 
# Generate list of commands for fastboot to execute
# ------------------------------------------------------- 
_flash_device() {

    # When lun0 flash is required, just to a factory flash
    if $factory_flash ; then
        _fastboot_flash "partition:0" $LUN0_PARTITION
        fastboot reboot-bootloader
        sleep 10
    fi

    
    local ROW="$(cursor-row)"
    for i in ${!flash_list[@]} ; do

        local NAME=${flash_list[i]}

        # Parse partMap entries and get KEY and VALUE pairs
        for part in "${partMap[@]}" ; do
            local KEY="${part%%::*}"
            local VALUE="${part##*::}"

            if [[ "${NAME}" == "${KEY}" ]] ; then
                local FILE="${VALUE}"
                break
            fi

        done

        __gotoxy 0 $ROW
        echo -e "${CLEAR_LINE}Flashing: ${NAME} | ${FILE}"

        __print_progress_bar ${i} "${#flash_list[@]}" 64
        echo
        _fastboot_flash $NAME $FILE

    done

    # kernel override specified with -k
    if $kernel_override ; then
        echo "[INFO] Kernel override specified, will flash: $KERNEL_OVERRIDE_FILE"
        _fastboot_flash "boot_a" $KERNEL_OVERRIDE_FILE
        _fastboot_flash "boot_b" $KERNEL_OVERRIDE_FILE
    fi

    __gotoxy 0 $ROW
    echo -e "${CLEAR_LINE}Done Flashing"
    __print_progress_bar 1 1 64
    echo

}

# ------------------------------------------------------- 
# Check if binaries to be flashed are present
# ------------------------------------------------------- 
_verify_binaries_exist() {
    # Compile list of all files to flash
    local files_list=( "${XBL_CONFIG}" "${XBL}" "${TZMBN}" "${NHLOS}" "${KERNEL}" "${SYSFS}" "${BOOTLOADER}" "${VER_INFO}" "${DSPSO}" )
    local file

    # Not all files are expected to be there, don't halt user for missing file
    # in situations where binary isn't required
    if $flash_data ; then
        files_list+=("${USRFS}")
    fi

    if $flash_cal ; then
        files_list+=("${CALFS}")
    fi

    if $flash_conf ; then
        files_list+=("${CONFS}")
    fi

    if $factory_flash ; then
        files_list+=("${LUN0_PARTITION}")
    fi

    # Check if all files exist in file system
    for file in ${files_list[@]} ; do
        if ! [ -f $file ] ; then
            echo "[ERROR] file is missing: {${file}}"
            exit -1
        fi
    done

    echo "[INFO] Found all required files"
    echo ""
}


## wait for a VOXL to be connected over adb or fastboot
_wait_for_device() {

    while true ; do
        if fastboot devices | grep -q fastboot ; then
            echo "[INFO] Found fastboot device"
            return 0
        fi
        if adb devices | grep -v List | grep -q device ; then
            echo "[INFO] Found ADB device"
            return 0
        fi
        sleep 1
    done
    return -1
}

# -------------------------------------------------------
# Verify user has fastboot and device is in fastboot mode
# -------------------------------------------------------
_put_device_in_fastboot() {
    # check if fastboot device populated serial number
    if fastboot devices | grep -q fastboot ; then
        return 0
    fi

    #check if adb device available
    if adb devices | grep -v List | grep -q device ; then
        echo "[INFO] Rebooting to fastboot"
        adb reboot-bootloader

        for i in {1..10}; do
            sleep 2
            if fastboot devices | grep -q fastboot ; then
                echo "[INFO] Found fastboot device"
                return 0
            fi
            echo "."
        done
        echo "[WARNING] Failed to reboot to fastboot"
        echo "Try power cycling VOXL while holding the fastboot button"
        return -1
    fi

    echo "[ERROR] No device found in adb or fastboot"

    return -1
}

# -------------------------------------------------------
# Make sure the correct GPT partition layout is flashed
# -------------------------------------------------------
_verify_partitions() {
    # The GPT 0 partition defines the partition layout for LUN0 physical partition which contains mostly
    # user filesystem partitions. Devices that haven't yet flashed this partition with our custom version
    # will be required to flash it.
    #
    # ModalAI Cal and Conf partitions being present will indicate that the correct LUN0 partition layout
    # has been flashed

    partitions=(
            "modalai_cal"
            "modalai_conf"
    )

    for partition in ${partitions[@]} ; do
        local output="$( fastboot getvar "partition-size:${partition}" 2>&1 )"

        echo "[INFO] checking if partition $partition exists"

        if echo $output | grep -q FAILED ; then
            # Normal, just need to flash whole table
            if echo "$output" | grep -q "GetVar Variable Not found" ; then
                echo "Partition: ${partition} not found"
                return -1
            else #Not normal, need to exit
                echo "Critical Fastboot error: ${partition} failed with error:"
                echo -en "\t"
                echo "$output" | grep FAILED | cut -d '(' -f 2-100 | rev | cut -c2- | rev
                echo
                echo "Make sure your device has been placed into fastboot properly"
                exit -1
            fi
        fi

        echo "[INFO] Found $partition partition"
    done

    return 0
}

_platform_warning() {
        # Identify device in order to make print more descriptive
        device=""
        if [[ $PLATFORM == "m0104" ]] ; then
            device="VOXL2 Mini"
        elif [[ $PLATFORM == "m0054" ]] ; then
            device="VOXL2"
        elif [[ $PLATFORM == "m0052" ]] ; then
            device="RB5 Flight"
        else
            echo "[ERROR] platform ${PLATFORM} is not supported. Must be either M0052 or M0054/M0104"
            exit -1
        fi

        font_color y
        echo "[WARNING] This system image flash is intended only for the following"
        echo "          platform:" $(font_color r)$device \($PLATFORM\) $(font_color y)
        echo ""
        echo "          Make sure that the device that will be flashed is correct."
        echo "          Flashing a device with an incorrect system image will lead"
        echo "          the device to be stuck in fastboot."
        echo ""
        font_color

        if [ ! $NON_INTERACTIVE ]; then 
            echo -e "Would you like to continue with the \033[1;31m$device ($PLATFORM)\033[0m system image flash?"
            select yn in "Yes" "No"; do
                case $yn in
                    Yes )
                        break
                        ;;
                    No )
                        exit -1
                        ;;
                esac
            done
        fi
}

_gen_partition_list() {

    # Check if partitions are correct, they'll need to be reflashed if not

    if ! _verify_partitions ; then
        font_color y
        echo "[WARNING] Partition layout was found to be incorrect, this requires"
        echo "          flashing the full partition table. Flashing the partition"
        echo "          table will also flash every partition causing all data on"
        echo "          the device to be lost."
        echo ""
        echo "$(font_color undrl)When doing a full flash, make sure to backup any important files$(font_color)"
        echo ""
        font_color

        factory_flash=true
        flash_list+=("userdata" "modalai_cal")

        if [ -z "$NON_INTERACTIVE" ]; then
            echo "Would you like to continue with the full wipe/flash now?"
            select yn in "Yes" "No"; do
                case $yn in
                    Yes )
                        break
                        ;;
                    No )
                        echo "Factory flash cancelled by the user."
                        exit -1
                        ;;
                esac
            done
        fi

    # Check if we're doing a factory flash. If we are go ahead and flash all the partitions including the data, conf, cal, and lun0 partition
    elif $factory_flash; then
        font_color y
        echo "[WARNING] You have selected the factory flash (-f / --factory) flag."
        echo "          This will flash all the partitions including the userdata,"
        echo "          calibration, and configuration partitions. You should only"
        echo "          continue if you are an experienced user as some factory   "
        echo "          data may not be able to be recovered."
        echo ""
        echo "$(font_color undrl)When doing a factory flash, make sure to backup any important files$(font_color)"
        echo ""
        font_color

        factory_flash=true
        flash_list+=("userdata" "modalai_cal")

        if [ -z "$NON_INTERACTIVE" ]; then
            echo "Would you like to continue with the factory flash now?"
            select yn in "Yes" "No"; do
                case $yn in
                    Yes )
                        break
                        ;;
                    No )
                        echo "Factory flash cancelled by the user."
                        exit -1
                        ;;
                esac
            done
        fi
    fi
}

# -------------------------------------------------------
# main fastboot logic
# -------------------------------------------------------
_flash_main() {
    _print_version

    echo ""
    echo "Please power off your VOXL, connect via USB,"
    echo "then power on VOXL. We will keep searching for"
    echo "an ADB or Fastboot device over USB"

    while true; do
        _wait_for_device
        if _put_device_in_fastboot ; then
            break
        fi
    done

    _platform_warning
    _gen_partition_list

    # Make sure required files are present
    _verify_binaries_exist

    _flash_device

    # Reboot device into adb and print relevant information
    fastboot reboot
    echo "[INFO] Waiting for ADB, if this takes more than 1 minute power cycle VOXL"
    adb wait-for-device
    echo "[INFO] Device ready, version:"
    adb shell "cat /etc/version"
    
    font_color g
    echo "[INFO] Finished flashing system image!"
    echo ""
    font_color
}

if [ -n "$TERM" ]; then
    __scrollable_clear_terminal
fi

_flash_main $@
